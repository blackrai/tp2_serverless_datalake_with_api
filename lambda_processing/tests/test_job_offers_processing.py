import os
from unittest import TestCase

import pandas as pd

from src.lambda_main_app import process_job_offers, enhance_date_and_create_date_partition


class TestJobOffersProcessing(TestCase):
    def setUp(self) -> None:
        test_file_path = os.path.join(os.path.dirname(__file__), "./data/job_offers.csv")
        self.job_offers = pd.read_csv(test_file_path, header=0, sep=",")

    def test_process_job_offers(self):
        # Given & When
        actual_df = process_job_offers(self.job_offers)
        # Then
        expected_cities = (
            "0             paris-la-defense\n"
            "1     paris-1er-arrondissement\n"
            "2     paris-1er-arrondissement\n"
            "3            neuilly-sur-seine\n"
            "4                     nanterre\n"
            "5                     toulouse\n"
            "6               cesson-sévigné\n"
            "7                        paris\n"
            "8                      loudéac\n"
            "9                         caen\n"
            "10                      labège\n"
            "11                      labège\n"
            "12                       paris\n"
            "13                       paris\n"
            "14                       paris\n"
            "15                       paris\n"
            "16             aix-en-provence\n"
            "17                       paris\n"
            "18               ile-de-france\n"
            "19                  la-défense\n"
            "20                       paris\n"
            "21                    toulouse\n"
            "22           neuilly-sur-seine\n"
            "23                       paris\n"
            "24                     vendôme\n"
            "25                       paris\n"
            "26         le-plessis-robinson\n"
            "27                       paris\n"
            "28                       paris\n"
            "29                       paris\n"
            "30                       paris\n"
            "31                       paris\n"
            "32                       paris\n"
            "33                        rome\n"
            "34                      mumbai\n"
            "35                       paris\n"
            "36                       paris\n"
            "37                       paris\n"
            "38                       paris\n"
            "39                    bordeaux"
        )
        assert actual_df["city"].to_string() == expected_cities

    def test_enhance_date_and_create_date_partition(self):
        # Given
        # When
        actual_df = enhance_date_and_create_date_partition(self.job_offers)
        # Then
        expected_partition_date = (
            ""
            "0     2019-10-14\n"
            "1     2019-10-31\n"
            "2     2019-10-31\n"
            "3     2019-10-31\n"
            "4     2019-10-24\n"
            "5     2019-10-22\n"
            "6     2019-10-21\n"
            "7     2019-10-21\n"
            "8     2019-08-27\n"
            "9     2019-10-16\n"
            "10    2019-10-15\n"
            "11    2019-10-15\n"
            "12    2019-10-14\n"
            "13    2019-10-14\n"
            "14    2019-10-14\n"
            "15    2019-10-28\n"
            "16    2019-10-02\n"
            "17    2019-10-02\n"
            "18    2019-09-30\n"
            "19    2019-09-20\n"
            "20    2019-09-19\n"
            "21    2019-09-16\n"
            "22    2019-09-16\n"
            "23    2019-10-11\n"
            "24    2019-08-22\n"
            "25    2019-10-31\n"
            "26    2019-09-17\n"
            "27    2019-10-15\n"
            "28    2019-10-15\n"
            "29    2019-10-15\n"
            "30    2019-10-15\n"
            "31    2019-10-15\n"
            "32    2019-10-11\n"
            "33    2019-10-19\n"
            "34    2019-10-19\n"
            "35    2019-10-07\n"
            "36    2019-10-07\n"
            "37    2019-10-31\n"
            "38    2019-10-31\n"
            "39    2019-10-31"
        )
        expected_timestamp = (
            "0     1.571039e+09\n"
            "1     1.572533e+09\n"
            "2     1.572533e+09\n"
            "3     1.572512e+09\n"
            "4     1.571921e+09\n"
            "5     1.571745e+09\n"
            "6     1.571660e+09\n"
            "7     1.571662e+09\n"
            "8     1.566902e+09\n"
            "9     1.571214e+09\n"
            "10    1.571149e+09\n"
            "11    1.571149e+09\n"
            "12    1.571043e+09\n"
            "13    1.571043e+09\n"
            "14    1.571043e+09\n"
            "15    1.572281e+09\n"
            "16    1.570021e+09\n"
            "17    1.570020e+09\n"
            "18    1.569855e+09\n"
            "19    1.568974e+09\n"
            "20    1.568906e+09\n"
            "21    1.568628e+09\n"
            "22    1.568627e+09\n"
            "23    1.570790e+09\n"
            "24    1.566471e+09\n"
            "25    1.572542e+09\n"
            "26    1.568738e+09\n"
            "27    1.571131e+09\n"
            "28    1.571131e+09\n"
            "29    1.571131e+09\n"
            "30    1.571131e+09\n"
            "31    1.571131e+09\n"
            "32    1.570790e+09\n"
            "33    1.571480e+09\n"
            "34    1.571480e+09\n"
            "35    1.570470e+09\n"
            "36    1.570470e+09\n"
            "37    1.572518e+09\n"
            "38    1.572518e+09\n"
            "39    1.572527e+09"
        )
        assert actual_df["date"].to_string() == expected_partition_date
        assert actual_df["timestamp"].to_string() == expected_timestamp
